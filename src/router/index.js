import Vue from 'vue';
import VueRouter from 'vue-router';
import vLoginForm from '../views/vLoginForm.vue';
import vChat from '../views/vChat.vue';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'home',
    component: vLoginForm,
  },
  {
    path: '/chat',
    name: 'chat',
    component: vChat,
  },
];

const router = new VueRouter({
  routes,
});

export default router;
