import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    authorization: false,
    socket: false,
    userName: '',
    userNameList: [],
    messageList: [],
  },
  mutations: {
    newConnection(state) {
      state.socket = new WebSocket(`ws://${window.location.hostname}:8082`);
      state.socket.onopen = () => {
        console.log('openConnetcion');
      };
      state.socket.onclose = () => {
        console.log('Close connection');
      };
    },
    setUserName(state, name) {
      state.userName = name;
      state.authorization = true;
    },
    setUserNameList(state, list) {
      state.userNameList = list;
    },
    pushMessage(state, data) {
      state.messageList.push({
        name: data.name,
        message: data.message,
        status: data.status || false,
      });
    },
    clearMessageList(state) {
      state.messageList = [];
    },
  },
  actions: {
    newConnection(context) {
      context.commit('newConnection');
    },
    setUserName(context, name) {
      context.commit('setUserName', name);
    },
    setUserNameList(context, list) {
      context.commit('setUserNameList', list);
    },
    pushMessage(context, data) {
      context.commit('pushMessage', data);
    },
    clearMessageList(context) {
      context.commit('clearMessageList');
    },
  },
  modules: {
  },
});
