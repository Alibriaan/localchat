/* eslint-disable no-trailing-spaces */
/* eslint-disable padded-blocks */
/* eslint-disable no-fallthrough */
/* eslint-disable func-names */
/* eslint-disable prefer-arrow-callback */
/* eslint-disable no-extra-semi */
/* eslint-disable no-unused-expressions */
/* eslint-disable import/no-extraneous-dependencies */

const WebSocket = require('ws');

const wss = new WebSocket.Server({ port: 8082 });
const arrName = [];

wss.on('connection', (ws) => {

  ws.on('message', (event) => {
    const data = JSON.parse(event);

    switch (data.type) {
      case 'newName':
      {
        if (!arrName.length || arrName.includes(data.name) === false) {
          arrName.push(data.name);
          const sendData = JSON.stringify({ type: 'SuccessName', name: data.name });
          ws.send(sendData);

          wss.clients.forEach(function (client) {
            if (client.readyState === WebSocket.OPEN) {
              client.send(JSON.stringify({ type: 'sendUserList', list: arrName }));
              if (client !== ws && client.readyState === WebSocket.OPEN) {
                client.send(JSON.stringify({ type: 'sendMessageList', name: data.name, status: 'join' }));
              } 
            };
          });
        } else {
          ws.send(JSON.stringify({ type: 'FailName' }));
        }
        break;
      }
      case 'closeConnection': {
        const position = arrName.indexOf(data.name);
        (position !== -1) ? arrName.splice(position, 1) : null;
        
        if (data.name) {
          wss.clients.forEach(function (client) {
            if (client.readyState === WebSocket.OPEN) {
              client.send(JSON.stringify({ type: 'sendMessageList', name: data.name, status: 'leave' }));
              client.send(JSON.stringify({ type: 'sendUserList', list: arrName }));
            };
          });
        }
        break;
      }
      case 'sendMessage': 
      {
        wss.clients.forEach(function (client) {
          if (client.readyState === WebSocket.OPEN) {
            client.send(JSON.stringify({
              type: 'sendMessageList', name: data.name, message: data.message, status: 'message', 
            }));
          };
        });
        break;
      }
      default:
      {
        break;
      }
    }

  });

  ws.on('close', () => {
  });
});
